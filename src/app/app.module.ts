import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AddIssueComponent } from './components/add-issue/add-issue.component';
import { EditIssueComponent } from './components/edit-issue/edit-issue.component';
import { IssueListComponent } from './components/issue-list/issue-list.component';
import { BugService } from './shared/bug.service';

// decorator - a typescript feature used to modify the behavior of classes, methods, proprties, or parameters at design time. They allow you to add metadata or modify the structure of the target element.
@NgModule({
  // declarations: components, directives, and pipes that belong to the module. These are avaialble for use within the module.
  declarations: [
    AppComponent,
    AddIssueComponent,
    EditIssueComponent,
    IssueListComponent,
  ],
  // imports: specify the other modules that this module depends on.
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  // providers: Specifies the services or dependencies that the module makes available for injection. Services defined here can be injected into components, directives, or other services within the module.
  providers: [BugService],

  bootstrap: [AppComponent],
})
export class AppModule {}
