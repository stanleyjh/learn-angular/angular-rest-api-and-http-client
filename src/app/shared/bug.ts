// Interface defining data types.

export class Bug {
  id: string;
  issue_name: string;
  issue_message: string;
}
